import React, { Component } from 'react'
import Child from './Child'

export default class Parent extends Component {
  constructor(){
    super();
    this.state={
        name:"swapnil",
        roll:10,
        count1:0,
        other_name:"rakesh",
        toggle:true
    };
  }
  handleClick=()=>
  {
    this.setState({roll:this.state.roll+1});
    this.setState({count1:this.state.count1+1});
    console.log(this.state.count1);


  };
handleChild=()=>
{   
    this.setState({toggle:false});

};

  render() {
    return (
      <div>
        <h1 >hello you are inside a parent component..</h1>
        <h3 >hello {this.state.name} your Roll no is {this.state.roll}
        hello other {this.state.other_name} your Roll is{this.state.count1}</h3>
        <button className='btn btn-primary'onClick={this.handleClick}>click me</button>
        <button onClick={this.handleChild}>Unmount Child</button>
        <hr/>
       
        {this.state.toggle && <Child some={this.state}/>}
        {!(this.state.toggle) && <h1>Child Unmounted</h1>}
      </div>
    )
  }
}
