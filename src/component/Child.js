import React, { cloneElement, Component } from 'react'
//
export default class Child extends Component {
  constructor(props){
    super(props);
    console.log("1st method :mounting phase :constructor loaded..and this will not render in update..");
    this.state={
        child_roll:this.props.some.roll,
        child_name:"jethalal",
        count:this.props.some.count1

    };
  }
  componentWillUnmount(){
    console.log("unmountig phase:this is the only methode.. child component unmounted");
  }
  componentDidMount(){
    console.log("4th method :mounting phase :componentdidmount() loaded last method in mounting phase..");
  //  console.log(this.state.count);
  }
  static getDerivedStateFromProps(props ,state){
    if(props.some.count1===0){
        if(props.some.roll!==state.child_roll)
    { 
        console.log("2nd method mounting phase :static getDerivedStateFromProps() loaded..state updated ");
    //    console.log(props, state);
        return{ child_roll:props.some.roll, count:props.some.count1};} 

    //console.log(props, state);
    console.log("2nd method mounting phase :static getDerivedStateFromProps() loaded..state not updated ");
        return null; 
    }
    else{if(props.some.roll!==state.child_roll)
        { 
            console.log("updating phase 1st method :static getDerivedStateFromProps() loaded"+props.some.count1+" time..state updated ");
      //      console.log(props, state);
            return{ child_roll:props.some.roll, count:props.some.count1};} 
    
        //console.log(props, state);
        console.log("updating phase 1st method :static getDerivedStateFromProps() loaded"+props.some.count1+" time..state not  updated ");
            return null; }
}
 shouldComponentUpdate(nextProps , nextState){
    if(this.state.child_roll<15){
        console.log("updating phase:2nd method.. shouldComponentUpdate() loaded "+(this.props.some.count1+1)+"time");
        //console.log(nextProps, nextState);
        
        return true;}
    else{
        console.log("updating phase: 2nd method..shouldComponentUpdate() loaded "+(this.props.some.count1+1)+"time and this will not allow the render to load and accordingly getsnapshot and componentDidUpdate will also not load.." );
    //console.log(nextProps, nextState);
        return false;
    }
    
 }
 getSnapshotBeforeUpdate(prevProps, prevState)
 {
    console.log("updating phase:4th method.. getSnapshotBeforeUpdate() loaded"+this.state.count+"time");
   // console.log(prevProps , prevState);
    return "done";
 }
componentDidUpdate(prevProps , prevState , c)
{
    console.log("updating phase:5th method.. Componentdidupdate() loaded"+this.state.count+"time");
    //console.log(prevProps , prevState);
}
handleThis=()=>
{
    this.setState({count:this.state.count+1});
};
    render() {
        if(this.state.count===0)
        console.log("3rd method. mounting phase:render loaded... ");
        else
        console.log("updating phase:3rd method :render loaded... "+this.state.count+"time");
    return (
      <div>
        <h1>hello you are inside child component..</h1>
        <h2>hello {this.state.child_name} your roll is {this.state.child_roll} and count is{this.state.count} and props count1 {this.props.some.count1}</h2>
        <button onClick={this.handleThis}>click</button>
      </div>
    )
  }
}
